import 'package:coolmovies/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Global{

  static Future<void> setUserData(String userId, String userName, BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Constants.USER_ID, userId);
    prefs.setString(Constants.USER_NAME, userName);
    Navigator.pushNamedAndRemoveUntil(context, '/movies', (route) => false);
  }

  static Future<String?> getUserId() async{
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(Constants.USER_ID);
  }

  static Future<String?> getUserName() async{
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(Constants.USER_NAME);
  }

  static void clearLoginData() async{
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(Constants.USER_ID);
    prefs.remove(Constants.USER_NAME);
  }
}