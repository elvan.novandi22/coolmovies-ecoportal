import 'package:coolmovies/models/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../CustomColor.dart';
import 'detail_movie_component.dart';
import 'movie_item_component.dart';

class CompleteDetailMovieComponent extends StatelessWidget{
  final Movie data;
  final bool? enableClose;

  CompleteDetailMovieComponent({required this.data, this.enableClose = true});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Card(
          color: Colors.white,
          child: MovieItemComponent(data.title!, enableClose!? GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: const Icon(Icons.close, size: 12,),
          ): null, data.releaseDateFormatted),
        ),
        Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              DetailMovieComponent(callback: (){

              }, componentText: data.movieDirectorByMovieDirectorId!.name!, componentIcon: Icons.movie, componentIconColor: CustomColor.blackColor, componentTextColor: CustomColor.blackColor),
              DetailMovieComponent(callback: (){
                if(enableClose!){
                  Navigator.pushNamed(context, "/reviews", arguments: data.id).then((value){
                    Navigator.pop(context);
                  });
                }
              }, componentText: data.getTotalReview, componentIcon: Icons.comment, componentIconColor: CustomColor.blueColor, componentTextColor: CustomColor.blackColor),
              DetailMovieComponent(callback: (){

              }, componentText: data.totalRating, componentIcon: Icons.star, componentIconColor: CustomColor.yellowColor, componentTextColor: CustomColor.yellowColor, componentTextFontWeight: FontWeight.bold,),
            ],
          ),
        )
      ],
    );
  }

}