import 'package:coolmovies/component/positive_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../CustomColor.dart';
import 'mouse_event_area.dart';

class LoginOrRegisterComponent extends StatelessWidget{
  final TextEditingController controller;
  final TextInputtedCallback btnCallback;
  final Color btnColor;
  final String btnText;
  final bool isRegisterMode;

  const LoginOrRegisterComponent(this.controller, this.btnCallback, this.btnColor, this.btnText, this.isRegisterMode, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextField(
          controller: controller,
          decoration: const InputDecoration(hintText: "Name"),
        ),
        PositiveButton(
          function: () {
            btnCallback(controller.text);
          },
          label: btnText,
          color: btnColor,
        ),
        isRegisterMode? Container():Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text(
              "Do not have account yet?",
              style: TextStyle(
                  color: CustomColor.blackColor,
                  fontSize: 12,
                  fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              width: 8,
            ),
            MouseEventArea(
                onTap: () {
                  Navigator.pushNamed(context, "/register");
                },
                child: const Text(
                  "Register",
                  style: TextStyle(
                      color: CustomColor.blueColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ))
          ],
        )
      ],
    );
  }

}

typedef TextInputtedCallback = void Function(
    String totalPrice, );