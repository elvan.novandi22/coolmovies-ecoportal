
import 'package:coolmovies/CustomColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MovieItemComponent extends StatelessWidget{
  String movieTitle;
  Widget? rightWidget;
  String releaseDate;
  MovieItemComponent(this.movieTitle, this.rightWidget, this.releaseDate, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(movieTitle, style: const TextStyle(color: CustomColor.yellowColor, fontWeight: FontWeight.bold, fontSize: 16),),
                const SizedBox(height: 4,),
                Text(releaseDate, style: const TextStyle(color: CustomColor.blackColor, fontWeight: FontWeight.normal, fontSize: 12, fontStyle: FontStyle.italic),)
              ],
            ),
            rightWidget != null? rightWidget! : Container()
          ],
        ));
  }

}