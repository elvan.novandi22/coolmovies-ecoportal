import 'package:flutter/cupertino.dart';

class DetailMovieComponent extends StatelessWidget{
  final VoidCallback callback;
  final String componentText;
  final Color componentTextColor;
  final FontWeight? componentTextFontWeight;
  final IconData componentIcon;
  final Color componentIconColor;

  DetailMovieComponent({required this.callback, required this.componentText, required this.componentIcon, required this.componentIconColor, required this.componentTextColor,
  this.componentTextFontWeight = FontWeight.normal});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        callback.call();
      },
      child: Container(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width/3
        ),
        padding: EdgeInsets.all(8),
        child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(componentIcon, color: componentIconColor,),
          const SizedBox(height: 8,),
          Text(componentText, style: TextStyle(color: componentTextColor, fontSize: 12, fontWeight: componentTextFontWeight),)
        ],
      ),),
    );
  }

}