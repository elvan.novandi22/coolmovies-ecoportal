import 'package:coolmovies/CustomColor.dart';
import 'package:coolmovies/component/positive_button.dart';
import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/models/movie_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class DialogEnterReview extends StatefulWidget{
  final Movie movie;
  final MovieReview? review;
  final String reviewerId;
  DialogEnterReview({required this.movie, this.review, required this.reviewerId});

  @override
  State<StatefulWidget> createState() {
    return DialogEnterReviewPageState();
  }

}

class DialogEnterReviewPageState extends State<DialogEnterReview>{
  var reviewController = TextEditingController();
  var bodyController = TextEditingController();
  final ValueNotifier<Map<String, dynamic>?> submitReviews = ValueNotifier(null);

  var rating = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.review != null){
      setState(() {
        reviewController.text = widget.review!.title!;
        bodyController.text = widget.review!.body!;
        rating = widget.review!.rating!;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(12),
      child: ValueListenableBuilder(valueListenable: submitReviews, builder: (BuildContext context, Map<String, dynamic>? data, Widget? _){
        if(data != null){
          Navigator.pop(context);
        }
        return Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap:(){
                  Navigator.pop(context);
                },
                child: Icon(Icons.close, size: 16, color: CustomColor.blackColor,),
              ),
            ),
            TextField(controller: reviewController,
              decoration: const InputDecoration(hintText: "Enter your review title"),
              onChanged: (text){
                setState(() {

                });
              },),
            TextField(controller: bodyController,
              decoration: const InputDecoration(hintText: "Description"),
              minLines: 3,
              maxLines: 5,
              onChanged: (text){
                setState(() {

                });
              },),
            SizedBox(height: 8,),
            Container(
              height: 40,
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, i){
                    return GestureDetector(
                      onTap: (){
                        setState(() {
                          rating = i+1;
                        });
                      },
                      child: i<rating?
                      Icon(Icons.star, size: 32, color: CustomColor.yellowColor,) :
                      Icon(Icons.star_border, size: 32, color: CustomColor.yellowColor,),
                    );
                  }),
            ),
            PositiveButton(
              enabled: reviewController.text.isNotEmpty && bodyController.text.isNotEmpty && rating != 0,
              function: (){
                if(widget.review == null){
                  submitData(reviewController.text, bodyController.text, rating, widget.movie.id, widget.reviewerId);
                } else {
                  editReview(widget.review!.nodeId!, widget.review!.id, reviewController.text, bodyController.text, rating, widget.movie.id, widget.reviewerId);
                }
              }, label: "SUBMIT", color: CustomColor.yellowColor,)
          ],
        );
      }),
    );
  }

  void editReview(String nodeId, String movieReviewId, String title, String body, int rating, String movieId, String reviewerId) async {
    var client = GraphQLProvider.of(context).value;

    final QueryResult result = await client.query(QueryOptions(document: gql("""
            mutation{
	updateMovieReview(input : {nodeId : "$nodeId", movieReviewPatch:{
		id : "$movieReviewId",
		title : "$title",
		body : "$body",
		rating : $rating,
		movieId : "$movieId",
		userReviewerId : "$reviewerId"
	}}){
		movieReview{
			id
		}
	}
}
        """)));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      print("EDIT RESULT "+result.data.toString());
      submitReviews.value = result.data;
    }
  }

  void submitData(String title, String body, int rating, String movieId, String reviewerId) async {
    var client = GraphQLProvider.of(context).value;

    final QueryResult result = await client.query(QueryOptions(document: gql("""
            mutation {
  createMovieReview(input: {
    movieReview: {
      title: "$title",
      body: "$body",
      rating: $rating,
      movieId: "$movieId",
      userReviewerId: "$reviewerId"
    }})
  {
    movieReview {
      id
      title
      body
      rating
      movieByMovieId {
        title
      }
      userByUserReviewerId {
        name
      }
    }
  }
}
        """)));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      print("SUBMIT RESULT "+result.data.toString());
      submitReviews.value = result.data;
    }
  }

}