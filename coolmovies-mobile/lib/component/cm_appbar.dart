import 'package:coolmovies/CustomColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CmAppbar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final bool automaticallyImplyLeading;
  final Widget? leading;
  final List<Widget>? actions;

  const CmAppbar({
    required this.title,
    this.automaticallyImplyLeading = true,
    this.leading,
    this.actions,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
      ),
      titleSpacing: automaticallyImplyLeading ? 0 : null,
      centerTitle: false,
      backgroundColor: CustomColor.yellowColor,
      shadowColor: Colors.black.withOpacity(0.2),
      elevation: 5,
      automaticallyImplyLeading: automaticallyImplyLeading,
      leading: leading,
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}