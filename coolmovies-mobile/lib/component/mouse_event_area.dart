import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MouseEventArea extends StatelessWidget {
  final VoidCallback? onTap;
  final Widget child;

  const MouseEventArea({
    required this.child,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: child,
        onTap: onTap,
      ),
    );
  }
}