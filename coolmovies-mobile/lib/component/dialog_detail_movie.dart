import 'package:coolmovies/models/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../CustomColor.dart';
import 'complete_detail_movie_component.dart';
import 'detail_movie_component.dart';
import 'movie_item_component.dart';

class DialogDetailMovie extends StatefulWidget{
  final String id;
  DialogDetailMovie(this.id);

  @override
  State<StatefulWidget> createState() {
    return DialogDetailMovieState();
  }

}

class DialogDetailMovieState extends State<DialogDetailMovie>{
  final ValueNotifier<Movie?> detailMovieData = ValueNotifier(null);

  @override
  Widget build(BuildContext context) {
    _fetchData(widget.id);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      insetPadding: const EdgeInsets.all(16),
      child: ValueListenableBuilder(valueListenable: detailMovieData, builder: (BuildContext context, Movie? data, Widget? _){
        if(data != null){
          return CompleteDetailMovieComponent(data: data, enableClose: true,);
        } else {
          return Center(
            child: const CircularProgressIndicator(),
          );
        }
      }),
    );
  }

  void _fetchData(String id) async {
    var client = GraphQLProvider.of(context).value;

    final QueryResult result = await client.query(
        QueryOptions(
            document: gql("""
          query {
 movieById(id : "$id" ){
    id
		title
		releaseDate
		movieDirectorByMovieDirectorId{
			id
			name
			age
		}
		movieReviewsByMovieId{
			nodes{
				id
				rating
				title
			}
		}
	}
}	
        """)
        )
    );

    if (result.hasException) {
      print(result.exception.toString());
    }

    if(result.data != null) {
      detailMovieData.value = Movie.fromJson(result.data!['movieById']);
    }
  }

}