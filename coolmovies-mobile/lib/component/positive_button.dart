import 'package:coolmovies/CustomColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PositiveButton extends StatelessWidget {
  const PositiveButton({
    Key? key,
    required this.function,
    required this.label,
    this.icon,
    this.color = CustomColor.blueColor,
    this.labelColor = Colors.white,
    this.enabled = true,
    this.margin = const EdgeInsets.only(top: 12),
  }) : super(key: key);

  final VoidCallback? function;
  final Color color;
  final String label;
  final dynamic icon;
  final Color labelColor;
  final bool enabled;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: MaterialButton(
        color: color,
        onPressed: enabled ? function : null,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child:
                icon is IconData ? Icon(icon, color: labelColor) : icon,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                label,
                style: TextStyle(color: labelColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}
