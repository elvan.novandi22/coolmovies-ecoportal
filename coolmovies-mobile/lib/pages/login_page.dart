import 'package:coolmovies/CustomColor.dart';
import 'package:coolmovies/component/login_or_register_component.dart';
import 'package:coolmovies/component/mouse_event_area.dart';
import 'package:coolmovies/component/positive_button.dart';
import 'package:coolmovies/global.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  TextEditingController nameLoginController = TextEditingController();
  final ValueNotifier<Map<String, dynamic>?> loginData = ValueNotifier(null);
  final ValueNotifier<bool> isLoading = ValueNotifier(false);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(24),
        child: Center(
          child: ValueListenableBuilder(valueListenable: loginData, builder: (BuildContext context, Map<String, dynamic>? data, Widget? _){
            if(data != null){
              var listUser = data['nodes'] as List;
              isLoading.value = false;
              Future.delayed(Duration.zero, (){
                if(listUser.isNotEmpty){
                  var userData = data['nodes'][0];
                  Global.setUserData(userData['id'], userData['name'], context);
                } else {
                  final snackBar = SnackBar(
                    content: const Text('Account Not Found!'),
                    action: SnackBarAction(
                      label: 'Register',
                      onPressed: () {
                        Navigator.pushNamed(context, "/register");
                      },
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              });
            }
            return ValueListenableBuilder(valueListenable: isLoading, builder: (BuildContext context, bool load, _){
              return load?
              const CircularProgressIndicator() :
              LoginOrRegisterComponent(
                  nameLoginController, (text){
                isLoading.value = true;
                login(text);
              }, CustomColor.yellowColor, "LOGIN", false);
            },);
          }),
        ),
      )),
    );
  }

  void login(String name) async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(document: gql("""
          query AllUser (\$name: String!){
            allUsers(filter: {name : {equalTo : \$name}}) {
nodes {
      id
      name
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
  }
          }
        """), variables: {'name': name}));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      loginData.value = result.data!['allUsers'];
    }
  }
}
