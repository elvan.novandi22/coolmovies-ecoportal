import 'package:coolmovies/CustomColor.dart';
import 'package:coolmovies/component/cm_appbar.dart';
import 'package:coolmovies/component/complete_detail_movie_component.dart';
import 'package:coolmovies/component/dialog_enter_review.dart';
import 'package:coolmovies/component/positive_button.dart';
import 'package:coolmovies/global.dart';
import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/models/movie_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MovieReviewsPage extends StatefulWidget {
  final String id;

  MovieReviewsPage(this.id);

  @override
  State<StatefulWidget> createState() {
    return MovieReviewsPageState();
  }
}

class MovieReviewsPageState extends State<MovieReviewsPage> {
  final ValueNotifier<Movie?> detailMovieData = ValueNotifier(null);
  final ValueNotifier<bool> deleteReview = ValueNotifier(false);
  String? myId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Global.getUserId().then((value) {
      setState(() {
        print(myId);
        myId = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _fetchData(widget.id);
    return Scaffold(
      appBar: CmAppbar(title: "Reviews"),
      body: ValueListenableBuilder(valueListenable: deleteReview, builder: (BuildContext context, bool data, Widget? _){
        if(!data){
          return ValueListenableBuilder(
              valueListenable: detailMovieData,
              builder: (BuildContext context, Movie? data, Widget? _) {
                if (data != null) {
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        CompleteDetailMovieComponent(
                          data: data,
                          enableClose: false,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.all(8),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data.title! + " Reviews",
                                style: TextStyle(
                                    color: CustomColor.blueColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount:
                                  data.movieReviewsByMovieId!.nodes.length,
                                  itemBuilder: (context, i) {
                                    MovieReview review =
                                    data.movieReviewsByMovieId!.nodes[i];
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    review
                                                        .userByUserReviewerId!.name,
                                                    style: TextStyle(
                                                        color:
                                                        CustomColor.blackColor,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 16),
                                                  ),
                                                  SizedBox(
                                                    width: 8,
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: CustomColor.yellowColor,
                                                    size: 12,
                                                  ),
                                                  Text(review.rating.toString(),
                                                      style: TextStyle(
                                                          color: CustomColor
                                                              .yellowColor,
                                                          fontSize: 12))
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Text(
                                                review.title!,
                                                style: TextStyle(
                                                    color: CustomColor.blackColor,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                              Text(
                                                review.body!,
                                                style: TextStyle(
                                                    color: CustomColor.blackColor,
                                                    fontSize: 10),
                                              )
                                            ],
                                          ), flex: 3,),
                                          Flexible(child: myId != null &&
                                              review.userByUserReviewerId!.id ==
                                                  myId
                                              ? Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  showModalBottomSheet(context: context, builder: (context) => DialogEnterReview(movie: detailMovieData.value!, reviewerId: myId!, review: review,) ).then((value){
                                                    _fetchData(widget.id);
                                                  });
                                                },
                                                child: Icon(
                                                  Icons.edit,
                                                  size: 24,
                                                  color:
                                                  CustomColor.blackColor,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 12,
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  deleteMovieReview(review.id);
                                                },
                                                child: Icon(
                                                  Icons.delete,
                                                  size: 24,
                                                  color: Colors.red,
                                                ),
                                              )
                                            ],
                                          )
                                              : Container(), flex: 1,)
                                        ],
                                      ),
                                    );
                                  })
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: const CircularProgressIndicator(),
                  );
                }
              });
        } else{
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      }),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16),
        constraints: BoxConstraints(maxHeight: 80),
        child: PositiveButton(
            function: () {
              showModalBottomSheet(
                  context: context,
                  builder: (context) =>
                      DialogEnterReview(movie: detailMovieData.value!, reviewerId: myId!,)).then((value){
                        _fetchData(widget.id);
              });
            },
            label: "Add Review"),
      ),
    );
  }

  void deleteMovieReview(String id) async {
    deleteReview.value = true;
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(document: gql("""
          mutation{
      deleteMovieReviewById(input : {id : "$id"}){
        movieReview{
          id
        }
      }
    }
        """)));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      deleteReview.value = false;
      _fetchData(widget.id);
    }

  }

  void _fetchData(String id) async {
    var client = GraphQLProvider.of(context).value;

    final QueryResult result = await client.query(QueryOptions(document: gql("""
          query {
 movieById(id : "$id" ){
    id
		title
		releaseDate
		movieDirectorByMovieDirectorId{
			id
			name
			age
		}
		movieReviewsByMovieId{
			nodes{
				id
				rating
				title
				body
				nodeId
				userByUserReviewerId{
					name
					id
				}
			}
		}
	}
}	
        """)));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      detailMovieData.value = Movie.fromJson(result.data!['movieById']);
    }
  }
}
