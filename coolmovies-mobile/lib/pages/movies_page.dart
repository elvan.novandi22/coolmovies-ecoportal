import 'package:coolmovies/CustomColor.dart';
import 'package:coolmovies/component/dialog_detail_movie.dart';
import 'package:coolmovies/component/movie_item_component.dart';
import 'package:coolmovies/component/positive_button.dart';
import 'package:coolmovies/global.dart';
import 'package:coolmovies/models/list_movies_response.dart';
import 'package:coolmovies/models/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MoviesPage extends StatefulWidget{
  const MoviesPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MoviesPageState();
  }

}

class MoviesPageState extends State<MoviesPage>{

  final ValueNotifier<List<Movie>?> moviesData = ValueNotifier([]);
  String? name;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Global.getUserName().then((value){
      name = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    _fetchData();
    return Scaffold(
      body: SafeArea(child: ValueListenableBuilder(valueListenable: moviesData, builder: (BuildContext context, List<Movie>? data, Widget? _){
        if(data != null){
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                name != null? Container(
                  padding: EdgeInsets.all(16),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("Hi $name!", style: TextStyle(color: CustomColor.blackColor, fontSize: 20, fontWeight: FontWeight.bold),),
                        const Text("This movies for you", style: TextStyle(color: CustomColor.blackColor, fontSize: 16),)
                      ],
                    ),
                    PositiveButton(function: (){
                      Global.clearLoginData();
                      Navigator.pushNamedAndRemoveUntil(context, "/login", (route) => false);
                    }, label: "LOG OUT", color: Colors.red,)
                  ],
                ),): Container(),
                ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: data.length,
                    itemBuilder: (context, i){
                      Movie movie = data[i];
                      return GestureDetector(
                        onTap: (){
                          showDialog(context: context, builder: (context)=> DialogDetailMovie(movie.id)).then((value){
                            _fetchData();
                          });
                        },
                        child: Card(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                          ),
                          child: Column(
                            children: [
                              MovieItemComponent(movie.title!, Text(movie.getTotalReview, style: TextStyle(color: CustomColor.blackColor, fontSize: 12),), movie.releaseDateFormatted),
                              Container(
                                padding: EdgeInsets.all(4),
                                color: CustomColor.blueColor,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text("Detail", style: TextStyle(color: Colors.white, fontSize: 12, ),),
                                    const Icon(Icons.arrow_circle_down_rounded, color: Colors.white, size: 12,)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    })
              ],
            ),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      })),
    );
  }


  void _fetchData() async {
    var client = GraphQLProvider.of(context).value;

    final QueryResult result = await client.query(
        QueryOptions(
          document: gql(r"""
          query AllMovies {
            allMovies {
              nodes {
                id
                title
                movieDirectorId
                userCreatorId
                releaseDate
								movieReviewsByMovieId{
									nodes{
										id
										rating
									}
								}
              }
            }
          }
        """),
        )
    );

    if (result.hasException) {
      print(result.exception.toString());
    }

    if(result.data != null) {
      moviesData.value = ListMovieResponse.fromJson(result.data!['allMovies']).nodes;
    }
  }



}