import 'package:coolmovies/component/cm_appbar.dart';
import 'package:coolmovies/component/login_or_register_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../CustomColor.dart';
import '../global.dart';

class RegisterPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return RegisterPageState();
  }

}

class RegisterPageState extends State<RegisterPage>{
  TextEditingController nameRegisterController = TextEditingController();
  final ValueNotifier<Map<String, dynamic>?> registerData = ValueNotifier(null);
  final ValueNotifier<bool> isLoading = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CmAppbar(title: "Register"),
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: Center(
          child: ValueListenableBuilder(valueListenable: registerData, builder: (BuildContext context, Map<String, dynamic>? data, Widget? _){
            if(data != null){
              var user = data['user'];
              isLoading.value = false;
              Future.delayed(Duration.zero, (){
                Global.setUserData(user['id'], user['name'], context);
              });
            }
            return ValueListenableBuilder(valueListenable: isLoading, builder: (BuildContext context, bool load, _){
              return load?
              const CircularProgressIndicator() :
              LoginOrRegisterComponent(
                  nameRegisterController, (text){
                register(text);
              }, CustomColor.blueColor, "REGISTER", true);
            },);

          }),
        ),
      ),
    );
  }

  void register(String name) async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(document: gql("""
          mutation {
  createUser(input: {user: {name: "$name"}}) {
    user {
      id
      name
    }
  }
}
        """), variables: {'name': name}));

    if (result.hasException) {
      print(result.exception.toString());
    }

    if (result.data != null) {
      registerData.value = result.data!['createUser'];
    }
  }

}