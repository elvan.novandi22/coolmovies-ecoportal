import 'package:json_annotation/json_annotation.dart';

part 'reviewer.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Reviewer {
  String name;
  String id;


  Reviewer({
    required this.name,
    required this.id
  });

  factory Reviewer.fromJson(Map<String, dynamic> json) =>
      _$ReviewerFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewerToJson(this);
}