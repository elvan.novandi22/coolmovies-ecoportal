import 'package:coolmovies/models/movie_director.dart';
import 'package:coolmovies/models/movie_reviews_by_review_id.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Movie {
  String id;
  String? title;
  String? movieDirectorId;
  String? userCreatorId;
  String? releaseDate;
  MovieDirector? movieDirectorByMovieDirectorId;
  MovieReviewsByReviewId? movieReviewsByMovieId;

  Movie({
    required this.id,
    this.title,
    this.movieDirectorId,
    this.userCreatorId,
    this.releaseDate,
    this.movieDirectorByMovieDirectorId,
    this.movieReviewsByMovieId
  });

  factory Movie.fromJson(Map<String, dynamic> json) =>
      _$MovieFromJson(json);

  Map<String, dynamic> toJson() => _$MovieToJson(this);

  String get getTotalReview{
    return movieReviewsByMovieId!.nodes.length.toString() + " Reviews";
  }

  String get releaseDateFormatted {
    try {
      final date = DateTime.parse(releaseDate!);
      return "Release Date :"+DateFormat("dd MMMM yyyy").format(date);
    } catch (e) {
      print(e);
      return "Gagal memformat tanggal";
    }
  }

  String get totalRating{
    var totalRating = 0;
    movieReviewsByMovieId!.nodes.forEach((element) {
      totalRating += element.rating!;
    });
    var mean = totalRating/movieReviewsByMovieId!.nodes.length;
    var meanInString = mean.toString();
    return "$meanInString/5";
  }
}