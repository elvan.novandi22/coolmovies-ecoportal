// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reviewer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Reviewer _$ReviewerFromJson(Map<String, dynamic> json) => Reviewer(
      name: json['name'] as String,
      id: json['id'] as String,
    );

Map<String, dynamic> _$ReviewerToJson(Reviewer instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };
