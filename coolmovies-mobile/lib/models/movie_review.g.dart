// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieReview _$MovieReviewFromJson(Map<String, dynamic> json) => MovieReview(
      id: json['id'] as String,
      rating: json['rating'] as int?,
      title: json['title'] as String?,
      body: json['body'] as String?,
      nodeId: json['nodeId'] as String?,
      userByUserReviewerId: json['userByUserReviewerId'] == null
          ? null
          : Reviewer.fromJson(
              json['userByUserReviewerId'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MovieReviewToJson(MovieReview instance) =>
    <String, dynamic>{
      'id': instance.id,
      'rating': instance.rating,
      'title': instance.title,
      'body': instance.body,
      'nodeId': instance.nodeId,
      'userByUserReviewerId': instance.userByUserReviewerId,
    };
