import 'package:coolmovies/models/reviewer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_review.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class MovieReview {
  String id;
  int? rating;
  String? title;
  String? body;
  String? nodeId;
  Reviewer? userByUserReviewerId;


  MovieReview({
    required this.id,
    this.rating,
    this.title,
    this.body,
    this.nodeId,
    this.userByUserReviewerId
  });

  factory MovieReview.fromJson(Map<String, dynamic> json) =>
      _$MovieReviewFromJson(json);

  Map<String, dynamic> toJson() => _$MovieReviewToJson(this);

}