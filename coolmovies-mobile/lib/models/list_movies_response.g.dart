// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_movies_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListMovieResponse _$ListMovieResponseFromJson(Map<String, dynamic> json) =>
    ListMovieResponse(
      nodes: (json['nodes'] as List<dynamic>)
          .map((e) => Movie.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListMovieResponseToJson(ListMovieResponse instance) =>
    <String, dynamic>{
      'nodes': instance.nodes,
    };
