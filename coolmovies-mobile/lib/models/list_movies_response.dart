import 'package:coolmovies/models/movie.dart';
import 'package:json_annotation/json_annotation.dart';

part 'list_movies_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ListMovieResponse {
  List<Movie> nodes;


  ListMovieResponse({
    required this.nodes
  });

  factory ListMovieResponse.fromJson(Map<String, dynamic> json) =>
      _$ListMovieResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ListMovieResponseToJson(this);
}