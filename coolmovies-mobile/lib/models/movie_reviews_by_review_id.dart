import 'package:coolmovies/models/movie_review.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_reviews_by_review_id.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class MovieReviewsByReviewId {
  List<MovieReview> nodes;


  MovieReviewsByReviewId({
    required this.nodes,
  });

  factory MovieReviewsByReviewId.fromJson(Map<String, dynamic> json) =>
      _$MovieReviewsByReviewIdFromJson(json);

  Map<String, dynamic> toJson() => _$MovieReviewsByReviewIdToJson(this);
}