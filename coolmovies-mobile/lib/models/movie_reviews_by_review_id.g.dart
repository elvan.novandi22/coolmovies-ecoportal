// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_reviews_by_review_id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieReviewsByReviewId _$MovieReviewsByReviewIdFromJson(
        Map<String, dynamic> json) =>
    MovieReviewsByReviewId(
      nodes: (json['nodes'] as List<dynamic>)
          .map((e) => MovieReview.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MovieReviewsByReviewIdToJson(
        MovieReviewsByReviewId instance) =>
    <String, dynamic>{
      'nodes': instance.nodes,
    };
