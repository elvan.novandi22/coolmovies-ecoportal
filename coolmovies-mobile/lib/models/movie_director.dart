import 'package:json_annotation/json_annotation.dart';

part 'movie_director.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class MovieDirector {
  String id;
  int? age;
  String? name;


  MovieDirector({
    required this.id,
    this.age,
    this.name,
  });

  factory MovieDirector.fromJson(Map<String, dynamic> json) =>
      _$MovieDirectorFromJson(json);

  Map<String, dynamic> toJson() => _$MovieDirectorToJson(this);
}