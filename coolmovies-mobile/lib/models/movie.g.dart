// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Movie _$MovieFromJson(Map<String, dynamic> json) => Movie(
      id: json['id'] as String,
      title: json['title'] as String?,
      movieDirectorId: json['movieDirectorId'] as String?,
      userCreatorId: json['userCreatorId'] as String?,
      releaseDate: json['releaseDate'] as String?,
      movieDirectorByMovieDirectorId: json['movieDirectorByMovieDirectorId'] ==
              null
          ? null
          : MovieDirector.fromJson(
              json['movieDirectorByMovieDirectorId'] as Map<String, dynamic>),
      movieReviewsByMovieId: json['movieReviewsByMovieId'] == null
          ? null
          : MovieReviewsByReviewId.fromJson(
              json['movieReviewsByMovieId'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MovieToJson(Movie instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'movieDirectorId': instance.movieDirectorId,
      'userCreatorId': instance.userCreatorId,
      'releaseDate': instance.releaseDate,
      'movieDirectorByMovieDirectorId': instance.movieDirectorByMovieDirectorId,
      'movieReviewsByMovieId': instance.movieReviewsByMovieId,
    };
