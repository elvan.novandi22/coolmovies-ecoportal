// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_director.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDirector _$MovieDirectorFromJson(Map<String, dynamic> json) =>
    MovieDirector(
      id: json['id'] as String,
      age: json['age'] as int?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$MovieDirectorToJson(MovieDirector instance) =>
    <String, dynamic>{
      'id': instance.id,
      'age': instance.age,
      'name': instance.name,
    };
