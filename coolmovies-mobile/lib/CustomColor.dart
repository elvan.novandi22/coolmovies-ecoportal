import 'package:flutter/material.dart';

class CustomColor{
  static const blackColor = Color(0xFF353D40);
  static const yellowColor = Color(0xFFF2B138);
  static const blueColor = Color(0xFF003F63);
  static const canvasColor = Color(0xFFD9D9D9);
}